﻿using SM.CommonLib;
using System.Net.Http.Json;
using System.Reflection;
using System.Text;

const int messageMaxLength = 128;
int ordinalNum = 1;
ConfigData? config = ClientConfig.GetConfigData<ConfigData>()!;
int messagesCount = config.MessagesCount;

Console.WriteLine("*****Masseges sender client*****");
Console.WriteLine();

int msgSentCount = 0;
while (msgSentCount != config.MessagesCount)
{
    var message = new Message
    {
        OrdinalNum = ordinalNum++,
        Text = GetRandomText(messageMaxLength)
    };
    string? lastError = await SendMessage(message, config.RemoteUrl, config.RequestAttempts);
    if (lastError != null)
    {
        Console.WriteLine("Error(s) when accessing the server!");
        Console.WriteLine($"Last error: {lastError}");
        break;
    }
    if (messagesCount > 0)
        msgSentCount++;
    await Task.Delay(config.SendingPeriod);
}

#if DEBUG
Console.ReadLine();
#endif

#region Local functions
static async Task<string?> SendMessage(Message msg, string remoteUrl, int requestAttempts)
{
    using var httpClient = new HttpClient();
    var assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
    httpClient.DefaultRequestHeaders.Add("Client-Name", assemblyName);
    Console.Write($"Trying to send a message #{msg.OrdinalNum} ...");
    string? lastErrorMsg = null;
    for (int i = 0; i < requestAttempts; i++)
    {
        try
        {
            HttpResponseMessage response = await httpClient.PostAsJsonAsync(remoteUrl, msg);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine(" successfully!");
                return null;
            }
            lastErrorMsg = response.StatusCode.ToString();
        }
        catch (Exception e) 
        {
            lastErrorMsg = e.Message;
        }
        await Task.Delay(500);
    }
    return lastErrorMsg;
}

static string GetRandomText(int maxLength)
{
    var strBuilder = new StringBuilder();
    var random = new Random();
    var textLength = random.Next(maxLength);
    char letter;

    for (int i = 0; i <= textLength; i++)
    {
        double flt = random.NextDouble();
        int shift = Convert.ToInt32(Math.Floor(25 * flt));
        letter = Convert.ToChar(shift + 65);
        strBuilder.Append(letter);
    }

    return strBuilder.ToString();
}

#endregion
