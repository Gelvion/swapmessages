﻿
internal record ConfigData(string RemoteUrl, int MessagesCount, int SendingPeriod, int RequestAttempts);