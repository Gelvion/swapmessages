﻿using Microsoft.AspNetCore.SignalR.Client;
using SM.CommonLib;
using System.Reflection;

ConfigData? config = ClientConfig.GetConfigData<ConfigData>()!;
string assemblyName = Assembly.GetExecutingAssembly().GetName().Name!;
var cts = new CancellationTokenSource();
bool triedRecconect = false;

HubConnection connection = new HubConnectionBuilder()
    .WithServerTimeout(TimeSpan.FromSeconds(config.ServerTimeoutSec))
    .WithUrl(config.RemoteUrl, (opts) =>
        opts.Headers.Add("Client-Name", assemblyName))
    .Build();

connection.Closed += async (error) =>
{
    if (error == null || error.GetType() == typeof(TimeoutException))
    {
        Console.WriteLine("Connection closed");
        cts.Cancel();
        return;
    }

    Console.WriteLine($"Connection closed with error: {error.Message}");
    if (!triedRecconect)
    {
        await Task.Delay(1000);
        Console.WriteLine("Trying reconnect to the server...");
        await Connect(connection, config.ConnectAttempts);
        triedRecconect = true;
    }
};

connection.On<Message>("Notify", (message) => 
{
    Console.WriteLine(message);
});

Console.WriteLine("*****Messages listener client*****");
Console.WriteLine();

Console.WriteLine("Connecting to the server...");
var lastErrorMsg = await Connect(connection, config.ConnectAttempts);
if (lastErrorMsg != null)
{
    Console.WriteLine("Error establishing connection to server:");
    Console.WriteLine(lastErrorMsg);
    cts.Cancel();
}

try
{
    await Task.Delay(Timeout.Infinite, cts.Token);
}
catch
{
#if DEBUG
    Console.ReadLine();
#endif
}

static async Task<string?> Connect(HubConnection connection, int connectAttempts)
{
    string? lastErrorMsg = null;
    for (int i = 0; i < connectAttempts; i++)
    {
        try
        {
            await connection.StartAsync();
            Console.WriteLine("Connection established! Listen to incoming messages...");
            Console.WriteLine();
            return null;
        }
        catch (Exception e)
        {
            lastErrorMsg = e.Message;
        }
        await Task.Delay(1000);
    }
    return lastErrorMsg;
}



