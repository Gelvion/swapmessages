﻿using SM.CommonLib;
using System.Net.Http.Json;
using System.Reflection;

ConfigData? config = ClientConfig.GetConfigData<ConfigData>()!;
string remoteUrl = config.RemoteUrl;
int getHistoryDelaySec = config.GetHistoryDelaySec;
int hystoryFromLastMinutes = config.HystoryFromLastMinutes;
int requestAttempts = config.RequestAttempts;

Console.WriteLine("***** Messages history client *****");
Console.WriteLine();
Console.WriteLine($"After {getHistoryDelaySec} seconds there will be a request to receive messages from {hystoryFromLastMinutes} minutes ago"); 

await Task.Delay(TimeSpan.FromSeconds(getHistoryDelaySec));

using var httpClient = new HttpClient();
var assemblyName = Assembly.GetExecutingAssembly().GetName().Name;
httpClient.DefaultRequestHeaders.Add("Client-Name", assemblyName);
var uriBuilder = new UriBuilder(remoteUrl);
var dtNow = DateTime.Now;
var dtNowStr = dtNow.ToString("s");
var dtFromStr = dtNow.
	AddMinutes(-1 * hystoryFromLastMinutes).ToString("s");
var queryString = $"?start={dtFromStr}&end={dtNowStr}";
uriBuilder.Query = queryString;

string? lastErrorMsg = null;
for (int i = 0; i < requestAttempts; i++)
{
	try
	{
		HttpResponseMessage response = await httpClient.GetAsync(uriBuilder.Uri);
		if (response.IsSuccessStatusCode)
		{
			var messages = await response.Content.ReadFromJsonAsync<IEnumerable<Message>>();
			Display(messages!);
			lastErrorMsg = null;
			break;
		}
		lastErrorMsg = response.StatusCode.ToString();
	}
	catch (Exception e)
	{
		lastErrorMsg = e.Message;
	}
	await Task.Delay(500);
}

if (lastErrorMsg != null)
{
	Console.WriteLine("Error(s) when accessing the server!");
	Console.WriteLine($"Last error: {lastErrorMsg}");
}

#if DEBUG
Console.ReadLine();
#endif

static void Display(IEnumerable<Message> messages)
{
	if (!messages.Any())
		Console.WriteLine("No messages in such period!");

	foreach (var message in messages)
	{
		Console.WriteLine(message);
    }
}