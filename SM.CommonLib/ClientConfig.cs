﻿using Microsoft.Extensions.Configuration;

namespace SM.CommonLib
{
    public static class ClientConfig
    {
        public static T? GetConfigData<T>()
        {
            var environment = Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT");

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            if (environment != null && environment.Equals("dev", StringComparison.InvariantCultureIgnoreCase))
                builder = builder.AddJsonFile("appsettings.dev.json", optional: true);

            var config = builder.Build();

            return config.Get<T>();
        }
    }
}
