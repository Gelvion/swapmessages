﻿namespace SM.CommonLib
{
    public class Message
    {
        public int OrdinalNum { get; set; }
        public string? Text { get; init; }
        public DateTime? DateTimeLabel { get; set; }

        public override string ToString()
        {
            return String.Concat(
                $"Ordinal number: {OrdinalNum} \n",
                $"Date: {DateTimeLabel} \n",
                $"Message: {Text} \n");
        }
    }
}
