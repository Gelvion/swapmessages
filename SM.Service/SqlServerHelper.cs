﻿using System.Data.SqlClient;

namespace SM.Service
{
    public class SqlServerHelper
    {
        private static string? _connectionString;

        public static void InitDB(string connectionString, IWebHostEnvironment environment)
        {
            _connectionString = connectionString;

            CreateNotExistsMessagesDB();
            CreateNotExistsMessagesTable(environment);
        }
        private static void CreateNotExistsMessagesDB()
        {
            using var sqlConnection = new SqlConnection(_connectionString);
            var sqlCheckCreateDB =
                "IF NOT EXISTS (SELECT database_id FROM sys.databases where name = 'SwapMessages') " +
                "CREATE DATABASE SwapMessages";

            sqlConnection.Open();
            using (var command = new SqlCommand(sqlCheckCreateDB, sqlConnection))
            {
                var added = command.ExecuteNonQuery();
            }
        }

        private static void CreateNotExistsMessagesTable(IWebHostEnvironment env)
        {
            var sqlCreateTableFilePath = env.ContentRootPath + @"Script/MessagesTable.sql";
            using var sqlConnection = new SqlConnection(_connectionString);
            sqlConnection.Open();
            var scriptCheckCreateTable = File.ReadAllText(sqlCreateTableFilePath);
            using (var command = new SqlCommand(scriptCheckCreateTable, sqlConnection))
            {
                var added = command.ExecuteNonQuery();
            }
        }
    }
}
