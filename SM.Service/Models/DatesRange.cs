﻿namespace SM.Service.Models;
public record DatesRange(DateTime? Start, DateTime? End);