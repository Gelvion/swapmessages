﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using SM.CommonLib;

namespace SM.Service.Models
{
    public class MessageDAL : IMessageDAL
    {
        private readonly string _connectionString;
        private SqlConnection? _sqlConnection;

        public MessageDAL(string connectionString)
        {
            _connectionString = connectionString;
        }
        private void OpenConnection()
        {
            _sqlConnection = new SqlConnection() { ConnectionString = _connectionString };
            _sqlConnection.Open();
        }

        private void CloseConnection()
        {
            if (_sqlConnection?.State != ConnectionState.Closed)
            {
                _sqlConnection?.Close();
            }
        }

        public async Task Insert(Message message)
        {
            OpenConnection();
            string sql = "INSERT INTO [dbo].[Messages] (MessageText, DateTimeLabel, OrdinalNum) VALUES (@MessageText, @DateTimeLabel, @OrdinalNum)";
            using (var command = new SqlCommand(sql, _sqlConnection))
            {
                command.Parameters.AddWithValue("@MessageText", message.Text);
                command.Parameters.AddWithValue("@DateTimeLabel", message.DateTimeLabel);
                command.Parameters.AddWithValue("@OrdinalNum", message.OrdinalNum);
                await command.ExecuteNonQueryAsync();
            }
            CloseConnection();
        }

        public IEnumerable<Message> Get(DatesRange datesRange)
        {
            DateTime? dateTimeFrom = datesRange.Start;
            DateTime? dateTimeTo = datesRange.End;

            string sql = "SELECT * FROM MESSAGES";
            OpenConnection();
            using (var command = new SqlCommand(sql, _sqlConnection))
            {
                if ((dateTimeFrom != null) && (dateTimeTo != null))
                {
                    sql = string.Concat(sql, $" WHERE (DateTimeLabel >= @dateTimeFrom AND DateTimeLabel <= @dateTimeTo)");
                    command.Parameters.AddWithValue("@dateTimeFrom", dateTimeFrom);
                    command.Parameters.AddWithValue("@dateTimeTo", dateTimeTo);
                }
                else if ((dateTimeFrom != null))
                {
                    sql = string.Concat(sql, $" WHERE DateTimeLabel >= @dateTimeFrom");
                    command.Parameters.AddWithValue("@dateTimeFrom", dateTimeFrom);
                }
                else if (dateTimeTo != null)
                {
                    sql = string.Concat(sql, $" WHERE DateTimeLabel <= @dateTimeTo");
                    command.Parameters.AddWithValue("@dateTimeTo", dateTimeTo);
                }
                command.CommandType = CommandType.Text;
                command.CommandText = sql;
                var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
                while (dataReader.Read())
                {
                    yield return new Message()
                    {
                        Text = (string)dataReader["MessageText"],
                        DateTimeLabel = (DateTime)dataReader["DateTimeLabel"],
                        OrdinalNum = (int)dataReader["OrdinalNum"]
                    };
                }
                dataReader.Close();
            }
        }
    }
}
