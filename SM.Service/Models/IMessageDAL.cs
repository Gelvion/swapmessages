﻿using SM.CommonLib;

namespace SM.Service.Models
{
    public interface IMessageDAL
    {
        public Task Insert(Message message);
        public IEnumerable<Message> Get(DatesRange datesRange);
    }
}
