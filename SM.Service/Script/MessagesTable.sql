﻿IF NOT EXISTS (SELECT object_id FROM sys.objects WHERE name='Messages' AND type = 'U')
    CREATE TABLE [dbo].[Messages]
    (
	    [Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
        [MessageText] NVARCHAR(128) NOT NULL, 
        [DateTimeLabel] DATETIME NOT NULL, 
        [OrdinalNum] INT NOT NULL
    )