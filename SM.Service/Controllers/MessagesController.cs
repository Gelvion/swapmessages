﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using SM.CommonLib;
using SM.Service.Models;

namespace MessagesSwapperService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessagesController : ControllerBase
    {
        private readonly ILogger<MessagesController> _logger;
        private readonly IHubContext<MessageHub> _hubContext;
        private readonly IMessageDAL _messageDAL;

        public MessagesController(ILogger<MessagesController> logger, IMessageDAL messageDAL, IHubContext<MessageHub> hubContext)
        {
            _logger = logger;
            _hubContext = hubContext;
            _messageDAL = messageDAL;
        }

        [HttpPost]
        public async Task Create([FromBody]Message message)
        {
            message.DateTimeLabel = DateTime.Now;
            await _messageDAL.Insert(message);
            _logger.LogInformation("New message #{0} added to database", message.OrdinalNum);
            await _hubContext.Clients.All.SendAsync("Notify", message);
            _logger.LogInformation("New added message #{0} was sent (via SignalR) to subscribers", message.OrdinalNum);
        }

        [HttpGet]
        public IEnumerable<Message> Get([FromQuery]DatesRange datesRange)
        {
            var messages = _messageDAL.Get(datesRange).ToList();
            _logger.LogInformation("Return {0} messages to the client", messages.Count());

            return messages;
        }
    }
}
