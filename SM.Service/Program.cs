using SM.Service;
using SM.Service.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddSignalR();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

string connection = builder.Configuration.GetConnectionString("DefaultConnection")!;
builder.Services.AddTransient<IMessageDAL, MessageDAL>(serviceProvider => new MessageDAL(connection));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapControllers();
app.MapHub<MessageHub>("/messageHub");

var logger = app.Services.GetService<ILogger<Program>>()!;
app.Use(async (context, next) =>
{
    var request = context.Request;
    var logMessage = $"Processing {request.Method} request to {request.Path}";

    var clientName = request.Headers["Client-Name"].ToString();
    if (string.IsNullOrEmpty(clientName))
    {
        var userAgent = request.Headers["User-Agent"].ToString();
        if (!String.IsNullOrEmpty(userAgent))
        {
            clientName = userAgent[0..(userAgent.IndexOf('(') - 1)];
        }
    }
    if (!string.IsNullOrEmpty(clientName))
        logMessage = $"{logMessage} from [{clientName}] client";
    logger.LogInformation(logMessage);
    
    await next.Invoke();
});

logger.LogInformation("Checking/creating [SwapMessages] database and [Messages] table...");
SqlServerHelper.InitDB(connection, app.Environment);

app.Run();
